# SfaPortal

PREREQUISITES:
1. Install the latest version of NodeJS from https://nodejs.org/en/download/
2. This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.30. Please install [angular-cli] to work with specific commands when generating Angular2 items.

## Automatically install packages
Run 'npm install' from the root folder of the app. All the packages will be installed in a new folder named 'node_modules'
Dependencies are located in the package.json file. Any new dependencies should be added in this file in the 'dependencies' section and the 'npm install' command should be executed afterwards.

## Run the app on a Development server
Run 'npm start' from the root folder of the project
Alternative: Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files. 

## Code scaffolding
Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build
Run 'npm start build:dev' to build the project. The build artifacts will be stored in the `dist/` directory. Use `:prod` for a production build.
Alternative: Run `ng build` to build the project.



## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests
Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Deploying to GitHub Pages
Run `ng github-pages:deploy` to deploy to GitHub Pages.

## Further help
To get more help on the `angular-cli` use `ng help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
