import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LoginService } from './shared/login.service';
import { UserService } from './shared/user.service';
import { User } from '../shared';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    user: User = new User();
    users: User[] = [];
    constructor(private router: Router, private userService: UserService, private loginService: LoginService) { }

    ngOnInit(): void {
        this.getUsers();
    }

    getUsers(): void {

    }

    login(username: string, password: string): void {
        this.userService.getUserId(username, password).subscribe(
            response => {
                this.loginService.setAuthToken(response.access_token),
                    this.gotoReports()
            },
            error => { console.log(error); }
        );
    }

    gotoReports(): void {
        this.router.navigate(['/reports']);
    }

}
