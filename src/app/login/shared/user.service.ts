import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { serverUrl } from '../../global';

@Injectable()
export class UserService {

  constructor(private http: Http) { }

  getUsers(): void { }

  getUser(): void { }

  getUserId(username: string, password: string): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    return this.http.post(`${serverUrl}oauth2/token`,
      `username=${username}&password=${password}&grant_type=password&client_id=resourceApi2`, { headers: headers })
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
}
