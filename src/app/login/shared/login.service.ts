import { Injectable } from '@angular/core';

@Injectable()
export class LoginService {
    private userId: number;
    private authToken: string;

    setLogedUserId(userId: number): void {
        this.userId = userId;
    }

    getLogedUserId(): number {
        return this.userId;
    }

    setAuthToken(authToken: string): void {
        this.authToken = `bearer ${authToken}`;
    }

    getAuthToken(): string {
        return this.authToken;
    }
}
