import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { Tab } from './tab';
import { Tabs } from './tabs';
import { BlankComponent } from './layouts/blank/blank.component';
import { BasicComponent } from './layouts/basic/basic.component';
import { NavigationComponent } from './navigation/navigation.component';
import { FooterComponent } from './footer/footer.component';
import { TopnavbarComponent } from './topnavbar/topnavbar.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule
    ],
    declarations: [
        Tab, Tabs,
        BlankComponent,
        BasicComponent,
        NavigationComponent,
        FooterComponent,
        TopnavbarComponent
    ],
    exports: [FormsModule, Tab, Tabs, BlankComponent, BasicComponent]
})
export class SharedModule { }
