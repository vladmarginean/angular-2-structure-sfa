export class Report {
    Id: number;
    SurveyId: number;
    ReportShareName: string;
    CreatedDateTime: Date;

    constructor() { }
}