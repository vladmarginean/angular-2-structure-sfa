import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BasicComponent, SharedModule } from '../shared';
import { ReportListComponent } from './report-list/report-list.component';
import { SiftReportComponent } from './sift-report/sift-report.component';

const routes: Routes = [
    {
        path: '', component: BasicComponent,
        children: [
            { path: 'reports', component: ReportListComponent },
            { path: 'reports/siftReport', component: SiftReportComponent }
        ]
    },
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule],
    providers: []
})
export class ReportsRoutingModule { }
