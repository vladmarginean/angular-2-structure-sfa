import { Injectable } from '@angular/core';
import { Response, Headers, Http, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { SiftReport } from '../../shared';
import { Report } from '../../shared';
import { serverUrl } from '../../global';
import { LoginService } from '../../login';

@Injectable()
export class ReportsService {
    constructor(private http: Http, private loginService: LoginService) {
    }

    getReportData(reportShareId: number, surveyId: number): Observable<any> {
        let headers = new Headers({ 'Authorization': this.loginService.getAuthToken() });
        return this.http.get(serverUrl + 'api/Report/SiftReport/' + `${reportShareId}/${surveyId}`, { headers: headers })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    getReports(): Observable<Report[]> {
        let headers = new Headers({ 'Authorization': this.loginService.getAuthToken() });
        return this.http.get(serverUrl + 'api/Report/ReportsForUser', { headers: headers })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
}
