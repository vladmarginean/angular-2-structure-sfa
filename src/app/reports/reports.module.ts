import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportsRoutingModule } from './reports-routing.module';
import { ReportListComponent } from './report-list/report-list.component';
import { SiftReportComponent } from './sift-report/sift-report.component';
import { ReportsService } from './shared/reports.service';
import { SharedModule } from '../shared';

@NgModule({
    imports: [
        CommonModule,
        ReportsRoutingModule,
        SharedModule
    ],
    declarations: [ReportListComponent, SiftReportComponent],
    exports: [ReportListComponent, SiftReportComponent],
    providers: [ReportsService]
})
export class ReportsModule { }
