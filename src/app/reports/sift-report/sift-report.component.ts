import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { SiftReport } from '../../shared';
import { ReportsService } from '../shared/reports.service';
import { Tab, Tabs } from '../../shared';

@Component({
    selector: 'app-sift-report',
    templateUrl: './sift-report.component.html',
    styleUrls: ['./sift-report.component.css']
})
export class SiftReportComponent implements OnInit {
    reportDataInvitees: SiftReport = new SiftReport();
    reportDataResponses: SiftReport = new SiftReport();

    constructor(private route: ActivatedRoute, private reportsService: ReportsService, private location: Location) { }

    ngOnInit(): void {
        this.route.queryParams.subscribe(queryParams => {
            this.reportsService.getReportData(+queryParams['id'], +queryParams['surveyId']).subscribe(
                reportData => {
                    this.reportDataInvitees = reportData.InviteesData,
                        this.reportDataResponses = reportData.ResponseData
                },
                error => { console.log(error); });
        });
    }

    goBack(): void {
        this.location.back();
    }
}
