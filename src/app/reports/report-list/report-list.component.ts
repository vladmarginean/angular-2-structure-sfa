import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Report } from '../../shared';
import { ReportsService } from '../shared/reports.service';
import { LoginService } from '../../login';

@Component({
    selector: 'app-report-list',
    templateUrl: './report-list.component.html',
    styleUrls: ['./report-list.component.css']
})
export class ReportListComponent implements OnInit {
    reports: Report[];
    constructor(
        private router: Router,
        private reportsService: ReportsService,
        private loginService: LoginService) { }

    ngOnInit(): void {
        this.reportsService.getReports().subscribe(
            reports => this.reports = reports,
            error => { console.log(error); });
    }

    gotoDetail(id: number, surveyId: number) {
        this.router.navigate(['reports/siftReport'], { queryParams: { id: id, surveyId: surveyId } });
    }

}
