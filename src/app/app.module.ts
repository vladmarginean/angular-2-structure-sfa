import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { ReportsModule } from './reports';
import { FeatureFlagModule } from './feature-flag';

import { routes } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent, UserService } from './login';
import { LoginService } from './login';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent
    ],
    imports: [
        // Angular modules
        BrowserModule,
        HttpModule,
        FormsModule,

        CoreModule,
        SharedModule,        
        // Feature modules
        ReportsModule,
        FeatureFlagModule,

        RouterModule.forRoot(routes)
    ],
    providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }, UserService, LoginService],
    bootstrap: [AppComponent],
    exports: []
})
export class AppModule { }
