import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { FeatureFlag } from '../shared';
import { LoginService } from '../login';
import { FeatureFlagService } from './shared/feature-flag.service';

@Component({
    selector: 'app-feature-flag',
    templateUrl: './feature-flag.component.html',
    styleUrls: ['./feature-flag.component.css']
})
export class FeatureFlagComponent implements OnInit {
    featureFlags: FeatureFlag[];
    constructor(
        private router: Router,
        private featureFlagService: FeatureFlagService,
        private loginService: LoginService,
        private location: Location) { }

    ngOnInit(): void {
        this.getFeatureFlags();
    }
    getFeatureFlags(): void {
        this.featureFlagService.getFeatureFlags().subscribe(
            featureFlags => this.featureFlags = featureFlags,
            error => { console.log(error); });
    }

    updateFeatureFlag(featureFlag: string, value: string): void {
        this.featureFlagService.updateFeatureFlag(featureFlag, value).subscribe(
            response => this.getFeatureFlags(),
            error => { console.log(error) });
    }

    goBack(): void {
        this.location.back();
    }
}
