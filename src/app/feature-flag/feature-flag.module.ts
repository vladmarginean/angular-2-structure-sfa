import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeatureFlagRoutingModule } from './feature-flag-routing.module';
import { FeatureFlagComponent } from './feature-flag.component';
import { FeatureFlagService } from './shared/feature-flag.service';
import { SharedModule } from '../shared';

@NgModule({
  imports: [
    CommonModule,
    FeatureFlagRoutingModule,
    SharedModule
  ],
  declarations: [FeatureFlagComponent],
  exports: [FeatureFlagComponent],
  providers: [FeatureFlagService]
})
export class FeatureFlagModule { }
