import { Injectable } from '@angular/core';
import { Response, Headers, Http, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { FeatureFlag } from '../../shared';
import { LoginService } from '../../login';
import { serverUrl } from '../../global';

@Injectable()
export class FeatureFlagService {
    constructor(private http: Http, private loginService: LoginService) {
    }

    getReportData(reportShareId: number, surveyId: number): Observable<any> {
        let headers = new Headers({ 'Authorization': this.loginService.getAuthToken() });
        return this.http.get(serverUrl + 'api/Report/SiftReport/' + `${reportShareId}/${surveyId}`, { headers: headers })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    getFeatureFlags(): Observable<FeatureFlag[]> {
        let headers = new Headers({ 'Authorization': this.loginService.getAuthToken() });
        return this.http.get(serverUrl + 'api/FeatureFlags', { headers: headers })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    updateFeatureFlag(featureFlag: string, value: string): Observable<any> {
        let bodyString = JSON.stringify({ "FeatureFlag": featureFlag, "Value": value });
        let headers = new Headers({ 'Authorization': this.loginService.getAuthToken(), 'Content-Type': 'application/json' });
        return this.http.post(serverUrl + 'api/FeatureFlags/UpdateFeatureFlag', bodyString, { headers: headers })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
        //              .subscribe(data => {
        //     data.json();
        // }
        // , error => {
        //     console.log(JSON.stringify(error.json()));
        // });
    }
}
