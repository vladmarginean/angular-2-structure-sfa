import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BasicComponent, SharedModule } from '../shared';
import { FeatureFlagComponent } from './feature-flag.component';

const routes: Routes = [
    {
        path: '', component: BasicComponent,
        children: [
            { path: 'featureFlag', component: FeatureFlagComponent }
        ]
    },
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule],
    providers: []
})
export class FeatureFlagRoutingModule { }
