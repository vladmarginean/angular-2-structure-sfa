import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { BlankComponent, SharedModule } from './shared';

export const routes: Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    {
        path: '', component: BlankComponent,
        children: [
            { path: 'login', component: LoginComponent }
        ]
    },
    {
        path: 'reports',
        loadChildren: 'app/reports/reports-routing.module#ReportsRoutingModule'
    },
    {
        path: 'featureFlag',
        loadChildren: 'app/feature-flag/feature-flag-routing.module#FeatureFlagRoutingModule'
    }
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
})

export class AppRoutingModule { }