import { SfaPortalPage } from './app.po';

describe('sfa-portal App', function() {
  let page: SfaPortalPage;

  beforeEach(() => {
    page = new SfaPortalPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
